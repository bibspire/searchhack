const bestand = require("./ballerup-bestand");
const popularity = require("./popularity");
const axios = require("axios");
axios.defaults.headers.post["Content-Type"] = "application/x-ndjson";

async function index() {
  try {
    await axios.delete("http://localhost:9200/searchhack/_all");
  } catch (e) {}

  try {
    await axios.put("http://localhost:9200/searchhack");
  } catch (e) {}

  let bulk = [];
  let indexCount = 0;
  async function processBulk() {
    const body = bulk.map(o => JSON.stringify(o) + "\n").join("");
    await axios.post("http://localhost:9200/searchhack/_bulk", body);
    indexCount += bulk.length / 2;
    bulk = [];
    console.log(`${new Date().toISOString()} indexed ${indexCount} documents`);
  }

  for (const wid of Object.keys(bestand)) {
    const lids = bestand[wid].map(s => +s);
    const meta = (await axios.get(`https://bibdata.dk/collection/${wid}.json`))
      .data;
    meta.hasPart = meta.hasPart.filter(o =>
      lids.includes(+o.pid.split(":")[1])
    );
    let image;
    meta.hasPart.forEach(o => (image = image || o.image));
    meta.image = image || meta.image;
    meta.popularity = (popularity[wid] || 0) + 1;

    bulk.push({ index: { _index: "searchhack", _id: String(wid) } });

    bulk.push({
      wid,
      name: meta.name,
      creator: meta.creator,
      contributor: meta.creator.concat(meta.contributor),
      keywords: meta.keywords,
      description: meta.description,
      image,
      hasPart: meta.hasPart.map(o => ({
        pid: o.pid,
        additionalType: o.additionalType
      })),
      datePublished: Math.min.apply(null, meta.datePublished),
      series: meta.isPartOf.map(o => o.name),
      popularity: meta.popularity
    });
    if (bulk.length >= 2000) {
      await processBulk();
    }
  }
  if (bulk.length) {
    await processBulk();
  }
}
index();
