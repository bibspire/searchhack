const express = require("express");
const fs = require("fs");
const axios = require("axios");
const app = express();
app.listen(process.env.PORT || 3000);
const index = fs.readFileSync("search.html", "utf-8");
axios.defaults.headers.post["Content-Type"] = "application/x-ndjson";
console.log('running search.bibspire.dk');

app.get("/", async (req, res) => {
  const query = req.query.q || "*";
  let result;
  try {
    result = await axios.post("http://localhost:9200/searchhack/_search", {
      size: 100,
      query: {
        function_score: {
          query: {
            multi_match: {
              query,
              fields: [
                "creator^40",
                "name^30",
                "keywords^5",
                "contributor^5",
                "series^5",
                "description" /*, 'additionalType'*/
              ],
              operator: "and"
            }
          },
          field_value_factor: {
            field: "popularity",
            factor: 0.1,
            modifier: "sqrt"
          }
        }
      }
    });
  } catch (e) {
    console.log(e.response.data);
  }
  result = result.data.hits.hits.map(o => o._source);
  res.end(
    index.replace(
      "RESULTS",
      result
        .filter(o => o.hasPart.length)
        .map(
          o => `<a href="https://bib.ballerup.dk/ting/object/${
            o.hasPart[0].pid
          }" class="result">
      <i class="img" style="background-image: url('${o.image}')" ></i>
      <div class="name">${o.name} ${
            o.datePublished
              ? ` <span class="date">(${o.datePublished})</span>`
              : ""
          }</div>
      <div class="creator">${o.creator.join(" &amp; ")}</div>
      <div>${o.keywords
        .map(s => `<span class="keyword">${s}</span>`)
        .join(" ")}</div>
    </a>
    `
        )
        .join("")
    )
  );
});
